import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/views/Home.vue';
import Contact from '@/views/Contact.vue';
import FlashCards from '@/views/FlashCards.vue';
import SoundCards from '@/views/SoundCards.vue';
import QuizBuddy from '@/views/QuizBuddy.vue';
import AddFlashCard from '@/views/AddFlashCard.vue';
import WordDetailCard from '@/views/Word.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact,
    },
    {
      path: '/flashCards',
      name: 'flashCards',
      component: FlashCards,
    },
    {
      path: '/soundCards',
      name: 'soundCards',
      component: SoundCards,
    },
    {
      path: '/quizBuddy',
      name: 'quizBuddy',
      component: QuizBuddy,
    },
    {
      path: '/addFlashCard',
      name: 'addFlashCard',
      component: AddFlashCard,
    },
    {
      path: '/words/:id',
      name: 'wordDetailCard',
      component: WordDetailCard,
    },
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/Contact.vue'),
    // },
  ],
});
