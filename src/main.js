import Vue from 'vue';
import VueSweetAlert2 from 'vue-sweetalert2';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
// As Vuetify components may use icons, we need below css
import 'material-design-icons-iconfont/dist/material-design-icons.css';

import App from './App.vue';
import router from './router';

Vue.use(VueSweetAlert2);
Vue.use(Vuetify);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app'); // #app is the anchor for rendering
