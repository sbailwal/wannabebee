/* eslint-disable func-names */

export default (function (window) {
  const AUDIO_RECORDER_WORKER = 'js/audioRecorderWorker.js';

  const AudioRecorder = function (source, cfg) { // constructor
    this.consumers = [];
    const config = cfg || {};
    const errorCallback = config.errorCallback || function () {};
    const inputBufferLength = config.inputBufferLength || 4096;
    const outputBufferLength = config.outputBufferLength || 4000;
    this.context = source.context;
    this.node = this.context.createScriptProcessor(inputBufferLength);
    const worker = new Worker(config.worker || AUDIO_RECORDER_WORKER);
    worker.postMessage({
      command: 'init',
      config: {
        sampleRate: this.context.sampleRate,
        outputBufferLength,
        outputSampleRate: (config.outputSampleRate || 16000),
      },
    });
    let recording = false;
    this.node.onaudioprocess = function (e) {
      if (!recording) return;
      worker.postMessage({
        command: 'record',
        buffer: [
          e.inputBuffer.getChannelData(0),
          e.inputBuffer.getChannelData(1),
        ],
      });
    };
    this.start = function (data) {
      this.consumers.forEach((consumer, y, z) => {
        consumer.postMessage({ command: 'start', data });
        recording = true;
        return true;
      });
      recording = true;
      return (this.consumers.length > 0);
    };
    this.stop = function () {
      if (recording) {
        this.consumers.forEach((consumer, y, z) => {
          consumer.postMessage({ command: 'stop' });
        });
        recording = false;
      }
      worker.postMessage({ command: 'clear' });
    };
    this.cancel = function () {
      this.stop();
    };

    const myClosure = this;
    worker.onmessage = function (e) {
      if (e.data.error && (e.data.error === 'silent')) errorCallback('silent');
      if ((e.data.command === 'newBuffer') && recording) {
        myClosure.consumers.forEach((consumer, y, z) => {
          consumer.postMessage({ command: 'process', data: e.data.data });
        });
      }
    };
    source.connect(this.node);
    this.node.connect(this.context.destination);
  }; // end constructor

  window.AudioRecorder = AudioRecorder; // TODO: try to remove this line and window param, see is it works
  return AudioRecorder;
}(window));
