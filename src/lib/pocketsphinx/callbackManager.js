/* eslint-disable func-names */

// // Originial with var >> let change
// (function(window){
//   let CallbackManager = function() {
// let currentId = 0;
// let callbackPool = {};
// this.add = function(clb) {
//     const id = currentId;
//     callbackPool[id] = clb;
//     currentId++;
//     return id;
// };
// this.get = function(id) {
//     if (callbackPool.hasOwnProperty(id)) {
//   const clb = callbackPool[id];
//   delete callbackPool[id];
//   return clb;
//     }
//     return null;
// };
//   };
//   window.CallbackManager = CallbackManager;

// })(window);

export default (function () {
  const CallbackManager = function () { // Constructor
    let currentId = 0;
    const callbackPool = {};
    this.add = function (clb) {
      const id = currentId;
      callbackPool[id] = clb;
      currentId += 1;
      return id;
    };
    this.get = function (id) {
      if (callbackPool.hasOwnProperty(id)) {
        const clb = callbackPool[id];
        delete callbackPool[id];
        return clb;
      }
      return null;
    };
  };

  window.CallbackManager = CallbackManager; // TODO: try to remove this line and window param, see is it works

  return CallbackManager;
}());

// export default { CallbackManager };


/*
  // export default (function(window) {
  //   const CallbackManager = function() {
  //     let currentId = 0;
  //     let callbackPool = {};
  //     this.add = function(clb) {
  //       let id = currentId;
  //       callbackPool[id] = clb;
  //       currentId++;
  //       return id;
  //     };
  //     this.get = function(id) {
  //       if (callbackPool.hasOwnProperty(id)) {
  //         var clb = callbackPool[id];
  //         delete callbackPool[id];
  //         return clb;
  //       }
  //       return null;
  //     };
  //   };
  //   window.CallbackManager = CallbackManager;
  //   return CallbackManager;
  // })(window); //execute passing windows

// module.exports.window.CallbackManager = CallbackManager;

// module.exports = {
//   window.CallbackManager: CallbackManager
// };
*/
/*

/////////////////////
// users.js
(
  function () {
    var users = ["Tyler", "Sarah", "Dan"]
    function getUsers() {
      return users
    }
    APP.getUsers = getUsers
  }
)()

///////////////////////

var users = ["Tyler", "Sarah", "Dan"]
function getUsers() {
  return users
}

module.exports.getUsers = getUsers

=============
var users = ["Tyler", "Sarah", "Dan"]
function getUsers() {
  return users
}

module.exports = {
  getUsers: getUsers
}
===================

*/
